import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Home from './routes/Home';
import ProductPage from './routes/ProductPage';



const App = () => (
    <Router>
        <div>
            <Route exact path='/' component={Home} />
            <Route path='/product/:productId' component={ProductPage} />
        </div>
    </Router>
);


export default App;
